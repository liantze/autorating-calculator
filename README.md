# Autorating Calculator

An autorating system uses peer ratings to award marks to individual students in team assignments.
This is a simple Autorating Calculator that will hopefully help implementing an "auto-rating" system for awarding individual marks in team assignments, based on peer ratings by the students themselves.

The following functions are provided:

* setting up group member lists 
* entering peer rating scores
* calculating autorated weights and scores
* exporting data to Excel

This tool was originally intended for personal use only, so please excuse the over-simplistic UX and bugs.