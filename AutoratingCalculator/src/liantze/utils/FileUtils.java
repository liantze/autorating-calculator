/**
 * 
 */
package liantze.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 * @author liantze
 *
 */
public class FileUtils {
	
	/**
	 * Gets a XML document from a file on disk.
	 * @param filename The file name to be opened.
	 * @return the XML Document.
	 */
    public static Document openXmlFile(String filename) {
        Document xmlDoc = null;
        SAXBuilder builder = new SAXBuilder();
        try {
            xmlDoc = builder.build(new InputStreamReader(new FileInputStream(
                    filename), "UTF-8"));
        } catch (JDOMException e) {
            System.err.println("Error opening XML file: ");
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            System.err.println("UTF-8 encoding not supported: ");
            e.printStackTrace();
            return null;
        } catch (FileNotFoundException e) {
            System.err.println("File " + filename + " not found: ");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Error opening XML file: ");
            e.printStackTrace();
        }

        return xmlDoc;
    }

    /**
     * Writes a JDOM XML document model to file in UTF-8 encoding. Existing
     * files with the same name will be overwritten!
     * 
     * @param xmlDoc
     *            The JDOM XML document to be saved.
     * @param filename
     *            The filename to write to.
     */
    public static void writeXMLFile(Document xmlDoc, String filename) {
        XMLOutputter outp = new XMLOutputter(Format.getPrettyFormat());
        try {
            outp.output(xmlDoc, new OutputStreamWriter(new FileOutputStream(
                    filename), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            System.err.println("UTF-8 encoding not supported: ");
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            System.err.println("File " + filename + " not found: ");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Error writing XML file: ");
            e.printStackTrace();
        }

    }

    /**
     * Gets a buffered file reader using UTF-8 encoding.
     * 
     * @param filename
     *            The file to read from
     * @return BufferedReader from <code>filename</code> using UTF-8 encoding.
     */
    public static BufferedReader getUTF8Reader(String filename) {
    	return getReader(filename, "UTF-8");
    }
    
    public static BufferedReader getReader(String filename, String charset) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(
                    new FileInputStream(new File(filename)), charset));
        } catch (UnsupportedEncodingException e) {
            System.err.println(charset + " encoding not supported: ");
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            System.err.println("File " + filename + " not found: ");
            e.printStackTrace();
        }
        return reader;    	
    }
    

    /**
     * Gets a plain text file writer using UTF-8 encoding. Existing files with
     * the same name will be overwritten!
     * 
     * @param filename
     *            The file to write to.
     * @return PrintWriter to <code>filename</code> using UTF-8 encoding.
     */
    public static PrintWriter getUTF8Writer(String filename) {
    	return getWriter(filename, "UTF-8");
    }
    
    public static PrintWriter getWriter(String filename, String charset) {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new OutputStreamWriter(
                    new FileOutputStream(new File(filename)), charset), true);
        } catch (UnsupportedEncodingException e) {
            System.err.println(charset + " encoding not supported: ");
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            System.err.println("File " + filename + " not found: ");
            e.printStackTrace();
        }
        return writer;
    }

}
