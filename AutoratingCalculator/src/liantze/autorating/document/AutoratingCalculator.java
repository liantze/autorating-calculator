/**
 * 
 */
package liantze.autorating.document;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jdom2.Element;

/**
 * @author liantze
 *
 */
public class AutoratingCalculator extends JFrame {
	GroupListDocument grouplistDoc;
	JPanel mainPanel = new JPanel();
	ActionJList studentJList;
	JTextField maxWeightTF;
	JTextField minWeightTF;
	JCheckBox allowSelfEvalCB;
	JTextField idTF;
	private static final String APP_NAME = "Autorating Calculator";

	static {
		try {
			UIManager.setLookAndFeel(
					UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
	}
	
	
	public AutoratingCalculator() {
		super("Autorating Calculator");
//		mainPanel.setLayout(new BorderLayout());
		this.setup();
		this.add(mainPanel);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		this.setPreferredSize(new Dimension(500,300));
		this.pack();
		this.setVisible(true);
	}
		
	
	public void importCSV(String csvfile, String xmlfile) {
		this.setTitle(AutoratingCalculator.APP_NAME + " - " 
	         + xmlfile.substring(xmlfile.lastIndexOf(System.getProperty("file.separator")) + 1));
		this.grouplistDoc = new GroupListDocument();
		this.grouplistDoc.importFrom(csvfile, xmlfile);
		this.grouplistDoc.printDoc();
		this.setupMiscFields();
		this.setupClass();
	}
	
	
	public void open(String xmlfile) {
		System.out.println(xmlfile);
		this.setTitle(AutoratingCalculator.APP_NAME + " - " 
		     + xmlfile.substring(xmlfile.lastIndexOf(System.getProperty("file.separator")) + 1));
		this.grouplistDoc = new GroupListDocument();
		this.grouplistDoc.openDoc(xmlfile);
		this.grouplistDoc.printDoc();
		this.setupMiscFields();
		this.setupClass();
		this.pack();
	}

	
	public void setup() {
		this.mainPanel.setLayout(new BorderLayout());
		this.setupMenu();
		this.pack();
	}
	
	
	public void setupMenu() {
		JMenuBar mbar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		fileMenu.setMnemonic('F');
		fileMenu.add(new OpenScoreFileAction(this));
		fileMenu.add(new SaveAction(this));
		fileMenu.addSeparator();
		fileMenu.add(new ImportGroupAction(this));
		fileMenu.add(new ExportCSVAction(this));
		fileMenu.addSeparator();
		fileMenu.add(new ExitAction(this));
				
		mbar.add(fileMenu);
		
		JMenu helpMenu = new JMenu("Help");
		helpMenu.setMnemonic('H');
		helpMenu.add(new AboutAction(this));
		mbar.add(helpMenu);

		this.setJMenuBar(mbar);
	}
	
	
	public void setupMiscFields() {
		this.maxWeightTF = new JTextField(3);
		this.minWeightTF = new JTextField(3);
		JLabel weightCapLbl = new JLabel("Limit weight from");
		JLabel weightCapMidLbl = new JLabel("to");
		weightCapLbl.setLabelFor(this.minWeightTF);
		weightCapMidLbl.setLabelFor(this.maxWeightTF);
		this.allowSelfEvalCB = new JCheckBox();
		JLabel allowSelfEvalLbl = new JLabel("Inlude self-evaluation");
		allowSelfEvalLbl.setLabelFor(this.allowSelfEvalCB);
		

		JPanel topStrip = new JPanel();
		topStrip.setLayout(new BoxLayout(topStrip, BoxLayout.Y_AXIS));
		JPanel firstRow = new JPanel();
		JPanel secondRow = new JPanel();
		firstRow.add(weightCapLbl);
		firstRow.add(this.minWeightTF);
		firstRow.add(weightCapMidLbl);
		firstRow.add(this.maxWeightTF);
		firstRow.add(Box.createRigidArea(new Dimension(30,0)));
		firstRow.add(this.allowSelfEvalCB);
		firstRow.add(allowSelfEvalLbl);
		this.idTF = new JTextField(5);
		secondRow.add(new JLabel("Search for ID"));
		secondRow.add(idTF);
		secondRow.add(new JButton(new SearchIDAction(this)));
//		secondRow.add(new JButton(new ApplySettingsAction(this)));
//		secondRow.add(new JButton(new OpenScoreFileAction(this)));
		topStrip.add(firstRow);
		topStrip.add(secondRow);
		this.mainPanel.add(topStrip, BorderLayout.NORTH);
	}
	
	public void setupClass() {
		if (grouplistDoc == null) {
			System.err.println("No students found, nothing to be done.");
			this.grouplistDoc = null;
			mainPanel.removeAll();
			return;
		}

		String[][] studentsArray = grouplistDoc.getStudentsArray();

		this.maxWeightTF.setText(
				Double.toString(this.grouplistDoc.getMaxWeight()));
		this.minWeightTF.setText(
				Double.toString(this.grouplistDoc.getMinWeight()));
		this.allowSelfEvalCB.setSelected(this.grouplistDoc.getAllowSelfEvaluation());

		String [] studentNames = new String[studentsArray.length];
		for (int i = 0; i < studentsArray.length; i++) {
			studentNames[i] = "[" + studentsArray[i][0] + "] " + studentsArray[i][1];
		}
		
		studentJList = new ActionJList(studentNames);
//		JTable studentJTable = new JTable(studentList, new String[] {"ID", "Name"});
//		studentJTable.setModel(tableModel);
		
		mainPanel.add(new JScrollPane(studentJList));
		
//		mainPanel.add(studentJList);
//		mainPanel.add(studentJTable);

		studentJList.addActionListener(  
				new ActionListener() {  
					public void actionPerformed(ActionEvent ae) {  
						String st = studentJList.getSelectedValue().toString();
						String selectedID = st.substring(st.indexOf('[') + 1, st.indexOf(']'));
						String selectedName = st.substring(st.indexOf("] ") + 2);
						System.out.println("Displaying group for: " + selectedName);
						new GroupPeerRatingDialog(AutoratingCalculator.this, selectedName, selectedID);
					}  
				});  
		this.pack();
	}
	
	public void saveSettings() {
		String minWeightText = this.minWeightTF.getText();
		String maxWeightText = this.maxWeightTF.getText();
		boolean allowSelfEval = this.allowSelfEvalCB.isSelected();
		if (minWeightText.length() > 0) {
			this.grouplistDoc.setMinWeight(Double.parseDouble(minWeightText));
		}
		if (maxWeightText.length() > 0) {
			this.grouplistDoc.setMaxWeight(Double.parseDouble(maxWeightText));
		}
		this.grouplistDoc.setAllowSelfEvaluation(allowSelfEval);
//		this.grouplistDoc.saveDoc();
	}
	
	public void exportCSV(String filename) {
		this.grouplistDoc.exportCSV(filename);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		AutoratingCalculator ac = new AutoratingCalculator();
//		ac.importCSV("CIT1224.csv", "CIT1224-Assgn1.xml"); // File > Import...
//		ac.open("CIT1224-Assgn1.xml"); // File > Open...
//		ac.exportCSV("CIT1224-Assgn1.csv"); // File > Export...
	}

}

class ApplySettingsAction extends AbstractAction {
	AutoratingCalculator ac;
	
	public ApplySettingsAction (AutoratingCalculator ac) {
		super("Apply Settings");
		this.ac = ac;
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		this.ac.saveSettings();
	}
	
}

class OpenScoreFileAction extends AbstractAction {
	AutoratingCalculator ac;

	public OpenScoreFileAction (AutoratingCalculator ac) {
		super("Open...");
		this.ac = ac;
		int mask = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();
		this.putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_O, mask));
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) {
		// open a JFileChooser
		JFileChooser fc = new JFileChooser();
		fc.setFileFilter(new FileNameExtensionFilter("XML", "xml"));
		int returnVal = fc.showOpenDialog(this.ac);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			String filepath = fc.getSelectedFile().getAbsolutePath();
			this.ac.mainPanel.removeAll();
			this.ac.open(filepath);
		}
	}
}

class SaveAction extends AbstractAction {
	AutoratingCalculator ac;
	
	public SaveAction (AutoratingCalculator ac) {
		super("Save");
		this.ac = ac;
		int mask = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();
		this.putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, mask));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.ac.saveSettings();
		this.ac.grouplistDoc.computeWeightsAndScores();
		this.ac.grouplistDoc.saveDoc();
	}
}


class ImportGroupAction extends AbstractAction {
	AutoratingCalculator ac;
	
	public ImportGroupAction (AutoratingCalculator ac) {
		super("Import class...");
		this.ac = ac;
		int mask = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();
		this.putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_I, mask));
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// open a JFileChooser
		JFileChooser fc = new JFileChooser();
		fc.setFileFilter(new FileNameExtensionFilter("Comma separated values (*.csv, *.txt)", "csv", "txt"));
		fc.setDialogTitle("Import groups...");
		int returnVal = fc.showOpenDialog(this.ac);
		if (returnVal != JFileChooser.APPROVE_OPTION) {
			return;
		}

		String csvfile = fc.getSelectedFile().getAbsolutePath();
		
		fc = new JFileChooser();
		fc.setFileFilter(new FileNameExtensionFilter("XML (*.xml)", "xml"));
		fc.setDialogTitle("Save new score file as...");
		returnVal = fc.showSaveDialog(this.ac);

		if (returnVal != JFileChooser.APPROVE_OPTION) {
			return;
		}
		
		String xmlfile = fc.getSelectedFile().getAbsolutePath();
		if (!xmlfile.endsWith(".xml")) {
			xmlfile = xmlfile + ".xml";
		}
		
		this.ac.mainPanel.removeAll();
		this.ac.importCSV(csvfile, xmlfile);
	}
}

class ExportCSVAction extends AbstractAction {
	AutoratingCalculator ac;

	public ExportCSVAction (AutoratingCalculator ac) {
		super("Export to CSV...");
		this.ac = ac;
		int mask = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();
		this.putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_E, mask));			
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// open a JFileChooser
		JFileChooser fc = new JFileChooser();
		fc.setFileFilter(new FileNameExtensionFilter("Comma separated values (*.csv)", "csv"));
		fc.setDialogTitle("Export to CSV...");
		int returnVal = fc.showSaveDialog(this.ac);
		if (returnVal != JFileChooser.APPROVE_OPTION) {
			return;
		}

		String csvfile = fc.getSelectedFile().getAbsolutePath();
		if (!csvfile.endsWith(".csv")) {
			csvfile = csvfile + ".csv";
		}
		this.ac.exportCSV(csvfile);
	}

}

class ExitAction extends AbstractAction {
	AutoratingCalculator ac;
	public ExitAction (AutoratingCalculator ac) {
		super("Exit");
		this.ac = ac;
		int mask = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();
		this.putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_Q, mask));			
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.ac.setVisible(false);
		this.ac.grouplistDoc = null;
		System.exit(0);
	}
}

class SearchIDAction extends AbstractAction {
	AutoratingCalculator ac;
	public SearchIDAction (AutoratingCalculator ac) {
		super("Search");
		this.ac = ac;
		int mask = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();
		this.putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F, mask));			
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		String id = ac.idTF.getText().trim();
		if (id.isEmpty()) {
			return;
		}
		
		Element student = this.ac.grouplistDoc.getStudentByID(id);
		if (student == null) {
			// display not found message and return
			JOptionPane.showMessageDialog(ac, 
					"ID " + id + " not found.", "No results", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		String name = student.getAttributeValue("name");
		this.ac.studentJList.setSelectedValue("[" + id + "] " + name, true);
		System.out.println("Displaying group for: " + name);
		new GroupPeerRatingDialog(ac, student.getAttributeValue("name"), id);
	}

}

class AboutAction extends AbstractAction {
	AutoratingCalculator ac;
	public AboutAction (AutoratingCalculator ac) {
		super("About");
		this.ac = ac;
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		JOptionPane.showMessageDialog(this.ac, 
				"Autorating Peer-Review Score Calculator"
				+ System.getProperty("line.separator")
				+ System.getProperty("line.separator")
				+ "� 2013 Lim Lian Tze"
				+ System.getProperty("line.separator")
				+ "liantze@gmail.com, liantze.lim@kdupg.edu.my",
				"Autorating Calculator", JOptionPane.INFORMATION_MESSAGE);
		
	}
}