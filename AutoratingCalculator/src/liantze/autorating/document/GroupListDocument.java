/**
 * 
 */
package liantze.autorating.document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.List;

import liantze.utils.FileUtils;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.filter.Filters;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.jdom2.xpath.XPathBuilder;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;

/**
 * @author liantze
 *
 */
public class GroupListDocument {
	
	Document xmlDoc;
	String ondiskFilename;
	static double DEFAULT_MAX_CAP = 1.02;
	static double DEFAULT_MIN_CAP = 0.8;
	static boolean DEFAULT_ALLOW_SELF_EVAL = true;

	static XPathBuilder<Element> findGroupBuilder = new XPathBuilder<Element>(
			"//group[student/@name = $student]", 
			Filters.element());
	static XPathBuilder<Element> findGroupOfIDBuilder = new XPathBuilder<Element>(
			"//group[student/@id = $id]", 
			Filters.element());
	
	static XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());


	static XPathBuilder<Element> findByNameBuilder = 
			new XPathBuilder<Element>("//student[@name = $student]", Filters.element());
	static XPathBuilder<Element> findByIDBuilder = 
			new XPathBuilder<Element>("//student[@id = $id]", Filters.element());
	static XPathBuilder<Element> ratedByIDBuilder =
			new XPathBuilder<Element>("rating[@by = $id]", Filters.element());

//	public GroupListDocument(String filename) {
//		this.xmlDoc = readFromCSVFile(filename);
//	}
	
	public GroupListDocument() {
	}
	
	/**
	 * Imports groups from a comma separated file with filename <pre>csvFilename</pre>.
	 * The XML should then be saved in <pre>ondiskFilename</pre>.
	 * @param csvFilename
	 */
	public void importFrom(String csvFilename, String ondiskFilename) {
		Document xml = new Document();
		xml.addContent(new Element("class"));
		
		BufferedReader reader = FileUtils.getUTF8Reader(csvFilename);
		String line;
		boolean newGroup = true;
		Element curGroup = null;
		try {
			while ((line = reader.readLine()) != null) {
				if (line.trim().length() == 0) {
					newGroup = true;
					continue;
				}
				
				if (newGroup) {
					curGroup = new Element("group");
					xml.getRootElement().addContent(curGroup);
					newGroup = false;
				}
				
				String[] tokens = line.trim().split(",");
				if (tokens.length < 2) {
					continue;
				}
				curGroup.addContent(
						new Element("student")
							.setAttribute("id", tokens[0].trim())
							.setAttribute("name" ,tokens[1].trim()));
			}
			
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.xmlDoc = xml;
		this.ondiskFilename = ondiskFilename;
		this.setAllowSelfEvaluation(DEFAULT_ALLOW_SELF_EVAL);
		this.setMaxWeight(DEFAULT_MAX_CAP);
		this.setMinWeight(DEFAULT_MIN_CAP);
		this.saveDoc();
	}
	
	public Document getDocument() {
		return this.xmlDoc;
	}
	
	public void printDoc(){
		System.out.println(outputter.outputString(xmlDoc.getDocument()));	
	}
	
	
	public void openDoc(String xmlfile) {
		this.xmlDoc = FileUtils.openXmlFile(xmlfile);
		this.ondiskFilename = xmlfile;
		this.setAllowSelfEvaluation(this.getAllowSelfEvaluation());
		this.setMaxWeight(this.getMaxWeight());
		this.setMinWeight(this.getMinWeight());
	}
	
	public void saveDoc() {
		FileUtils.writeXMLFile(this.xmlDoc, this.ondiskFilename);
	}
	
	public double getMaxWeight() {
//		return this.maxCap;
		Element maxCapElem = this.xmlDoc.getRootElement().getChild("max_cap");
		return maxCapElem == null ? DEFAULT_MAX_CAP
				: Double.parseDouble(maxCapElem.getText());
	}
	
	public void setMaxWeight(double cap) {
		if (cap < 1) {
			return;
		}
//		this.maxCap = cap;
		Element maxCapElem = this.xmlDoc.getRootElement().getChild("max_cap");
		if (maxCapElem == null) {
			this.xmlDoc.getRootElement().addContent(0,
					new Element("max_cap").setText(Double.toString(cap)));
		} else {
			maxCapElem.setText(Double.toString(cap));
		}
	}

	public double getMinWeight() {
//		return this.maxCap;
		Element minCapElem = this.xmlDoc.getRootElement().getChild("min_cap");
		return minCapElem == null ? DEFAULT_MIN_CAP
				: Double.parseDouble(minCapElem.getText());
	}
	
	public void setMinWeight(double cap) {
		if (cap > 1 || cap <= 0) {
			return;
		}
//		this.minCap = cap;
		Element minCapElem = this.xmlDoc.getRootElement().getChild("min_cap");
		if (minCapElem == null) {
			this.xmlDoc.getRootElement().addContent(0,
					new Element("min_cap").setText(Double.toString(cap)));
		} else {
			minCapElem.setText(Double.toString(cap));
		}
	}

	public boolean getAllowSelfEvaluation() {
//		return this.selfEvaluate;
		Element allowElem = this.xmlDoc.getRootElement().getChild("allow_self_eval");
		return allowElem == null ? DEFAULT_ALLOW_SELF_EVAL
				: Boolean.parseBoolean(allowElem.getText());
	}
	
	public void setAllowSelfEvaluation(boolean allow) {
//		this.selfEvaluate = allow;
		Element allowElem = this.xmlDoc.getRootElement().getChild("allow_self_eval");
		if (allowElem == null) {
			this.xmlDoc.getRootElement().addContent(0,
					new Element("allow_self_eval").setText(Boolean.toString(allow)));
		} else {
			allowElem.setText(Boolean.toString(allow));
		}
	}
	
	public List<Element> getStudentElems() {
		XPathBuilder<Element> allStudentsBuilder = new XPathBuilder<Element>("//student", Filters.element());
		XPathExpression<Element> allStudentsXPath = allStudentsBuilder.compileWith(XPathFactory.instance());
		return allStudentsXPath.evaluate(this.xmlDoc.getRootElement());
	}
	
	public String[][] getStudentsArray() {
		List<Element> studentElems = this.getStudentElems();
		if (studentElems == null || studentElems.isEmpty()) {
			return null;
		}
		
		String[][] studentList = new String[studentElems.size()][2];
//		String[] studentNames = new String[studentElems.size()];
		for (int i = 0; i < studentElems.size(); i++) {
			studentList[i][0] = studentElems.get(i).getAttributeValue("id");
			studentList[i][1] = studentElems.get(i).getAttributeValue("name");
//			studentNames[i] = studentElems.get(i).getText();
//			studentNames[i] = "[" + studentList[i][0] + "] " + studentList[i][1];
		}

		return studentList;
	}
	
	public Element getGroupOf(String student) {
		System.out.println("Looking for group mates of " + student + ".");
		findGroupBuilder.setVariable("student", student);
		XPathExpression<Element>  findGroupXPath = findGroupBuilder.compileWith(XPathFactory.instance());
		Element groupXML = findGroupXPath.evaluateFirst(this.xmlDoc.getRootElement());
		return groupXML;
	}		

	public Element getGroupOfID(String id) {
//		System.out.println("Looking for group mates of " + student + ".");
		findGroupOfIDBuilder.setVariable("id", id);
		XPathExpression<Element>  findGroupXPath = findGroupOfIDBuilder.compileWith(XPathFactory.instance());
		Element groupXML = findGroupXPath.evaluateFirst(this.xmlDoc.getRootElement());
		return groupXML;
	}		

	public void setGroupScore(Element group, double score) {
		if (group.getChild("overall_score") == null) {
			group.addContent(0, new Element("overall_score").setText(Double.toString(score)));
		} else {
			group.getChild("overall_score").setText(Double.toString(score));
		}
	}
	
	/**
	 * Updates the XML with <pre>rating</pre> score given by <pre>byStudentID</pre> to <pre>forStudentID</pre>.
	 * @param forStudentID
	 * @param byStudentID
	 * @param rating
	 */
	public void rate(String forStudentID, String byStudentID, double rating) {
		// find forStudentName
		Element student = getStudentByID(forStudentID);
		if (student == null) {
			System.err.println("Not found!!");
		}
		
		// check if child rating[@by = $byStudentID] already exist
		Element ratedBy = getRatingBy(byStudentID, student);
//		if (ratedBy == null) {
//			System.err.println("Not found!!");
//		}
		
		// if yes, update the score
		if (ratedBy != null) {
			ratedBy.setText(Double.toString(rating));
		} else {
		// if no, add new node
			ratedBy = new Element("rating").setAttribute("by", byStudentID).setText(Double.toString(rating));
			student.addContent(ratedBy);
		}
	}

	private Element getRatingBy(String id, Element student) {
//		System.out.println("Looking for by: " + id);
		ratedByIDBuilder.setVariable("id", id);
		XPathExpression<Element> findRatedPath = ratedByIDBuilder.compileWith(XPathFactory.instance());
		Element ratedBy = findRatedPath.evaluateFirst(student);
		return ratedBy;
	}

	public Element getStudentByID(String id) {
//		System.out.println("Looking for student: " + id);
		findByIDBuilder.setVariable("id", id);
		XPathExpression<Element> findStudentPath = findByIDBuilder.compileWith(XPathFactory.instance());
		Element student = findStudentPath.evaluateFirst(this.xmlDoc.getRootElement());
		return student;
	}
	
	/**
	 * Gets the peer rating score given by <pre>byStudnetID</pre> to
	 * <pre>forStudentID</pre> from the XML.
	 * @param forStudentID
	 * @param byStudentID
	 * @return
	 */
	public double getRating (String forStudentID, String byStudentID) {
		// find forStudentID
		Element student = this.getStudentByID(forStudentID);
		
		// then find rating byStudentID
		Element ratingBy = this.getRatingBy(byStudentID, student);
		return ratingBy == null ? -1 : Double.parseDouble(ratingBy.getText());
	}
	
	public double getGroupScore(String forStudentID) {
		Element groupXML = this.getGroupOfID(forStudentID);
		return this.getGroupScore(groupXML);
	}
	
	public double getGroupScore(Element groupXML) {
		String scoreStr = groupXML.getChildText("overall_score");
		return scoreStr == null ? -1 : Double.parseDouble(scoreStr);
	}
	
	public double getIndRating(String studentID) {
		Element st = this.getStudentByID(studentID);
		Element indRating = st.getChild("ind_rating");
		return indRating == null ? -1 : Double.parseDouble(indRating.getText());
	}
	
	public double getIndWeight(String studentID) {
		Element st = this.getStudentByID(studentID);
		Element indWeight = st.getChild("ind_weight");
		return indWeight == null ? -1 : Double.parseDouble(indWeight.getText());
	}

	public double getAdjustedWeight(String studentID) {
		Element st = this.getStudentByID(studentID);
		Element adjWeight = st.getChild("adj_weight");
		return adjWeight == null ? -1 : Double.parseDouble(adjWeight.getText());
	}

	public double getIndScore(String studentID) {
		Element st = this.getStudentByID(studentID);
		Element indScore = st.getChild("ind_score");
		return indScore == null ? -1 : Double.parseDouble(indScore.getText());
	}

	
	public double getGroupAvgRating(String studentID) {
		Element group = this.getGroupOfID(studentID);
		Element groupAvgRating = group.getChild("avg_rating");
		return groupAvgRating == null ? -1 : Double.parseDouble(groupAvgRating.getText());
	}

	public double getGroupAvgRating(Element groupXML) {
		Element groupAvgRating = groupXML.getChild("avg_rating");
		return groupAvgRating == null ? -1 : Double.parseDouble(groupAvgRating.getText());
	}
	
	public void computeWeightsAndScores() {
		List<Element> groups = this.xmlDoc.getRootElement().getChildren("group");
		for (Element g : groups) {
			this.computeWeightsAndScores(g);
		}
	}
	
	public void computeWeightsAndScores(Element groupXML) {
		DecimalFormat df = new DecimalFormat("#.00");
		
		// get the students
		List<Element> students = groupXML.getChildren("student");
		double sumIndRating = 0;
		for (int i = 0; i < students.size(); i++) {
			// get ratings for this student.
			Element st = students.get(i);
			List<Element> ratings = st.getChildren("rating");
			if (ratings.isEmpty()) {
				continue;
			}
			double ratingSum = 0;
			double numRating = 0;
			
			// compute average rating for each student
			for (int j = 0; j < ratings.size(); j++) {
				Element r = ratings.get(j);
				
				// ignore self-rating if specified
				if (!this.getAllowSelfEvaluation() && (i == j) ) {
					continue;
				}
				
				ratingSum += Double.parseDouble(r.getTextTrim());
				numRating++;
			}
			double avgRating = ratingSum / numRating;
			sumIndRating += avgRating;
			if (st.getChild("ind_rating") == null) {
				st.addContent(new Element("ind_rating").
					setText(df.format(avgRating)));
			} else {
				st.getChild("ind_rating").setText(df.format(avgRating));
			}
		}
		
		if (sumIndRating == 0) {
			return;
		}
		
		double avgGroupRating = sumIndRating / students.size();
		if (groupXML.getChild("avg_rating") == null) {
			groupXML.addContent(2, new Element("avg_rating").
					setText(df.format(avgGroupRating)));
		} else {
			groupXML.getChild("avg_rating").setText(df.format(avgGroupRating));
		}
		
		Element groupScoreElem = groupXML.getChild("overall_score");
		double groupScore = 
				(groupScoreElem == null ? -1 : Double.parseDouble(groupScoreElem.getText()) );
		for (Element st : students) {
			// ind. weight = ind. average / group average
			double indRating = Double.parseDouble(st.getChildText("ind_rating"));
			double indWeight = Double.valueOf(df.format(indRating / avgGroupRating));
			
			if (st.getChild("ind_weight") == null) {
				st.addContent(new Element("ind_weight").
						setText(df.format(indWeight)));
			} else {
				st.getChild("ind_weight").setText(df.format(indWeight));
			}
			
			// make sure weight is within limits
			indWeight = Math.min(this.getMaxWeight(), indWeight);
			indWeight = Math.max(this.getMinWeight(), indWeight);
			if (st.getChild("adj_weight") == null) {
				st.addContent(new Element("adj_weight").
						setText(df.format(indWeight)));
			} else {
				st.getChild("adj_weight").setText(df.format(indWeight));
			}
			
			if (groupScore >= 0) {
				if (st.getChild("ind_score") == null) {
					st.addContent(new Element("ind_score").
							setText(df.format(indWeight * groupScore)));
				} else {
					st.getChild("ind_score").setText(df.format(indWeight * groupScore));
				}
			}
		}
	}
	
	public void exportCSV(String filename) {
		PrintWriter writer = FileUtils.getUTF8Writer(filename);
		writer.println("ID,name,ind_rating,ind_weight,adj_weight,group_score,final_score");
		List<Element> students = this.getStudentElems();
		for (Element st : students) {
			writer.print(st.getAttributeValue("id"));
			writer.print(",");
			writer.print(st.getAttributeValue("name"));
			writer.print(",");
			
			Element child;
			child = st.getChild("ind_rating");
			writer.print(child == null ? "" : child.getTextTrim());
			writer.print(",");
			
			child = st.getChild("ind_weight");
			writer.print(child == null ? "" : child.getTextTrim());
			writer.print(",");
			
			child = st.getChild("adj_weight");
			writer.print(child == null ? "" : child.getTextTrim());
			writer.print(",");

			child = st.getParentElement().getChild("overall_score");
			writer.print(child == null ? "" : child.getTextTrim());
			writer.print(",");

			child = st.getChild("ind_score");
			writer.print(child == null ? "" : child.getTextTrim());
			
			writer.println();

		}
		writer.close();
	}

}
