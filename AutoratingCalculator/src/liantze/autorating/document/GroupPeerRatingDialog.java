/**
 * 
 */
package liantze.autorating.document;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import layout.SpringUtilities;

import org.jdom2.Element;

/**
 * @author liantze
 *
 */
public class GroupPeerRatingDialog extends JDialog {
//	AutoratingCalculator parent;
	GroupListDocument classDoc;
	String student;
	String studentID;
	Element groupXML;
	String[][] groupMembers;
	JTextField groupScore;
	JTextField[] ratingsGiven;
	JLabel[] ratingsReceived;
	JLabel[] indAvgRating;
	JLabel[] indWeight;
	JLabel[] indCappedWeight;
	JLabel[] indScore;
	JTabbedPane tabbedPane;
	
	public GroupPeerRatingDialog(AutoratingCalculator f, String student, String sid) {
		super(f, "Group Peer Ratings");
		f.saveSettings();
		this.student = student;
		this.studentID = sid;
//		this.parent = f;
		this.classDoc = f.grouplistDoc;
		this.groupMembers = getGroupMembersOf(sid);
		if (this.groupMembers == null) {
			System.err.println("Group not found??");
		}

		tabbedPane = new JTabbedPane();
		// create number of text fields for entering peer ratings given by 'student'.
		// read from XML if already existing values!
		tabbedPane.addTab("Peer Ratings Given by " + this.student, this.prepareRatingGivenForm());
		tabbedPane.addTab("Peer Ratings Received by " + this.student, this.prepareRatingReceivedForm());
		tabbedPane.addTab("Group Overview", this.prepareGroupView());
		groupScore = new JTextField(5);
		// also one field for overall group score. separate tab?
		
		this.add(tabbedPane);
		
		// check whether scores has already been entered
		// by this student before.
		this.updateFormFields();
		

		JPanel buttonStrip = new JPanel();
//		buttonStrip.setLayout(new BoxLayout(buttonStrip, BoxLayout.PAGE_AXIS));
		buttonStrip.add(new JButton(new CommitRatingsAction(this)));
		buttonStrip.add(new JButton(new ComputeWeightScoreAction(this)));
		this.add(buttonStrip, BorderLayout.SOUTH);
		
		JPanel groupScorePanel = new JPanel();
		groupScorePanel.add(new JLabel("Group score:"));
		groupScorePanel.add(groupScore);
		this.add(groupScorePanel, BorderLayout.NORTH);
		
		this.pack();
		this.setVisible(true);
	}
	
	private String[][] getGroupMembersOf(String id) {
		this.groupXML = this.classDoc.getGroupOfID(id);
		if (this.groupXML == null) {
//			System.out.println("nothing found");
			return null;			
		}
		
		List<Element> groupMemberElems = this.groupXML.getChildren("student");
		
		if (groupMemberElems == null || groupMemberElems.isEmpty()) {
//			System.out.println("nothing found");
			return null;
		}
		
		String[][] members = new String[groupMemberElems.size()][2];
		for (int i = 0; i < groupMemberElems.size(); i++) {
			members[i][0] = groupMemberElems.get(i).getAttributeValue("id");
			members[i][1] = groupMemberElems.get(i).getAttributeValue("name");
			System.out.println(members[i][1]);
		}
		System.out.println();
		
		// create labels and text fields
		
		return members;	
	}
	
	private JPanel prepareRatingGivenForm() {
		JPanel ratingPanel = new JPanel(new SpringLayout());
		ratingsGiven = new JTextField[this.groupMembers.length];
		JLabel[] memberNameLabels = new JLabel[this.groupMembers.length];
		for (int i = 0; i < this.groupMembers.length; i++) {
			memberNameLabels[i] = new JLabel(this.groupMembers[i][1] + ": ");
			ratingsGiven[i] = new JTextField(5);
			ratingsGiven[i].setMaximumSize(ratingsGiven[i].getPreferredSize());
			
			memberNameLabels[i].setLabelFor(ratingsGiven[i]);
			ratingPanel.add(memberNameLabels[i]);
			ratingPanel.add(ratingsGiven[i]);

			if (!this.classDoc.getAllowSelfEvaluation() && this.groupMembers[i][0].equals(this.studentID)) {
				ratingsGiven[i].setEditable(false);
//				ratingsGiven[i].setVisible(false);
			}
		}
		
		SpringUtilities.makeCompactGrid(ratingPanel,
                this.groupMembers.length, 2, //rows, cols
                5, 5,        //initX, initY
                5, 5);       //xPad, yPad
		return ratingPanel;
	}

	private JPanel prepareRatingReceivedForm() {
		JPanel ratingPanel = new JPanel(new SpringLayout());
//		ratingsReceived = new JTextField[this.groupMembers.length];
		ratingsReceived = new JLabel[this.groupMembers.length];
		JLabel[] memberNameLabels = new JLabel[this.groupMembers.length];
		for (int i = 0; i < this.groupMembers.length; i++) {
			// anonymous names
//			memberNameLabels[i] = new JLabel("Vote " + (i+1) + ": ");
			
			// real names
			memberNameLabels[i] = new JLabel(this.groupMembers[i][1] + ": ");
			ratingsReceived[i] = new JLabel("N/A");
			ratingsReceived[i].setMaximumSize(ratingsReceived[i].getPreferredSize());
			
			memberNameLabels[i].setLabelFor(ratingsReceived[i]);
			ratingPanel.add(memberNameLabels[i]);
			ratingPanel.add(ratingsReceived[i]);

			if (!this.classDoc.getAllowSelfEvaluation() && this.groupMembers[i][0].equals(this.studentID)) {
				ratingsReceived[i].setVisible(false);
			}
		}
		
		SpringUtilities.makeCompactGrid(ratingPanel,
                this.groupMembers.length, 2, //rows, cols
                5, 5,        //initX, initY
                5, 5);       //xPad, yPad
		return ratingPanel;
	}
	
	public void updateFormFields() {
		DecimalFormat df = new DecimalFormat("#0.00");
		double overallScore = this.classDoc.getGroupScore(this.groupXML);
		if (overallScore >= 0) {
			groupScore.setText(df.format(overallScore));
		}
		
		double retValue;
		for (int i = 0; i < this.groupMembers.length; i++) {
			// check ratings given by current member to other members
			retValue = this.classDoc.getRating(this.groupMembers[i][0], this.studentID);
//			System.out.println("rating = " + rating);
			if (retValue >= 0) {
				ratingsGiven[i].setText(df.format(retValue));
			}
			
			// check ratings given by other members to current member
			retValue = this.classDoc.getRating(this.studentID, this.groupMembers[i][0]);
//			System.out.println("rating = " + rating);
			if (retValue >= 0) {
				ratingsReceived[i].setText(df.format(retValue));
			}

			retValue = this.classDoc.getIndRating(this.groupMembers[i][0]);
			if (retValue >=0) {
				indAvgRating[i].setText(df.format(retValue));
			}
			retValue = this.classDoc.getIndWeight(this.groupMembers[i][0]);
			if (retValue >=0) {
				indWeight[i].setText(df.format(retValue));
			}
			retValue = this.classDoc.getAdjustedWeight(this.groupMembers[i][0]);
			if (retValue >=0) {
				indCappedWeight[i].setText(df.format(retValue));
			}
			retValue = this.classDoc.getIndScore(this.groupMembers[i][0]);
			if (retValue >=0) {
				indScore[i].setText(df.format(retValue));
			}

		}
		
	}
	
	private JPanel prepareGroupView() {
		JPanel groupWeightPanel = new JPanel(new SpringLayout());
		JLabel[] memberNameLabels = new JLabel[this.groupMembers.length];
		indAvgRating = new JLabel[this.groupMembers.length];
		indWeight = new JLabel[this.groupMembers.length];
		indCappedWeight = new JLabel[this.groupMembers.length];
		indScore = new JLabel[this.groupMembers.length];
		
		groupWeightPanel.add(new JLabel("Group Member"));
		groupWeightPanel.add(new JLabel("Avg. Rating"));
		groupWeightPanel.add(new JLabel("Weight"));
		groupWeightPanel.add(new JLabel("Adj. Weight"));
		groupWeightPanel.add(new JLabel("Final Score"));

		
		for (int i = 0; i < this.groupMembers.length; i++) {
			memberNameLabels[i] = new JLabel(this.groupMembers[i][1]);
			indAvgRating[i] = new JLabel("N/A", SwingConstants.RIGHT);
			indWeight[i] = new JLabel("N/A", SwingConstants.RIGHT);
			indCappedWeight[i] = new JLabel("N/A", SwingConstants.RIGHT);
			indScore[i] = new JLabel("N/A", SwingConstants.RIGHT);
			
			
//			memberNameLabels[i].setLabelFor(ratingsReceived[i]);
			groupWeightPanel.add(memberNameLabels[i]);
			groupWeightPanel.add(indAvgRating[i]);
			groupWeightPanel.add(indWeight[i]);
			groupWeightPanel.add(indCappedWeight[i]);
			groupWeightPanel.add(indScore[i]);
		}
		
		SpringUtilities.makeCompactGrid(groupWeightPanel,
                this.groupMembers.length + 1, 5, //rows, cols
                5, 5,        //initX, initY
                15, 5);       //xPad, yPad
		return groupWeightPanel;
	}	
	
	/**
	 * Save ratings and group scores to XML model (and write to file).
	 */
	public void saveRatings() {
		// save group score
		this.classDoc.setGroupScore(this.groupXML,
				Double.parseDouble(this.groupScore.getText()));
		// save each rating
		for (int i = 0; i < this.groupMembers.length; i++) {
			this.classDoc.rate(this.groupMembers[i][0], this.studentID, 
					Double.parseDouble(this.ratingsGiven[i].getText()));
		}
		
		this.classDoc.saveDoc();
//		System.out.println("**** Saving ratings ****");
//		this.classDoc.printDoc();
//		System.out.println("************************");	
	}
	
	public void computeAllWeightsScores() {
		this.classDoc.saveDoc(); // horrible to keep saving to disk. but what the heck.
		this.classDoc.computeWeightsAndScores();
		this.classDoc.saveDoc();
	}
	
	public void computeWeightsScores() {
		this.classDoc.saveDoc();
		this.classDoc.computeWeightsAndScores(this.groupXML);
		this.classDoc.saveDoc();
	}
}

class CommitRatingsAction extends AbstractAction {
	GroupPeerRatingDialog gprd;
	
	public CommitRatingsAction(GroupPeerRatingDialog p) {
		super("Commit Ratings");
		this.gprd = p;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.gprd.saveRatings();
		this.gprd.updateFormFields();
//		this.gprd.setVisible(false);
	}
}

class ComputeWeightScoreAction extends AbstractAction {
	GroupPeerRatingDialog gprd;

	public ComputeWeightScoreAction(GroupPeerRatingDialog p) {
		super("Compute Weights and Scores");
		this.gprd = p;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.gprd.computeWeightsScores();
		this.gprd.updateFormFields();
	}
}

